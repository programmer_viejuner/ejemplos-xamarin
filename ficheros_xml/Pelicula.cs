using System;
namespace xamarin_practica56.Models
{
    
    public class Pelicula
    {
        public int Precio { get; set; }
        public String Distribuidor { get; set; }
        public String Director { get; set; }
        public String Argumento { get; set; }
        public String Imagen { get; set; }
    }
}
