using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using xamarin_practica56.Models;


namespace xamarin_practica56
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            this.btn_buscar.Clicked += Botonleeralumnos_Clicked;


        }

        private void Botonleeralumnos_Clicked(object sender, EventArgs e)
        {
            var assembly = IntrospectionExtensions.GetTypeInfo(typeof(MainPage)).Assembly;
            Stream stream = assembly.GetManifestResourceStream("xamarin_practica56.Ficheros.Novedades.xml");
            string contenido = "";
            using (var reader = new System.IO.StreamReader(stream))
            {
                contenido = reader.ReadToEnd();
            }

            XDocument docxml = XDocument.Parse(contenido);
            var consulta = from datos in docxml.Descendants("TITULOS") where datos.Attribute("PELICULA").Value == box_titulo.Text

            select new Pelicula
            {
                Precio = int.Parse(datos.Element("PRECIO").Value)
                ,
                Distribuidor = datos.Element("DISTRIBUIDOR").Value
                ,
                Director = datos.Element("DIRECTOR").Value
                ,
                Argumento = datos.Element("ARGUMENTO").Value
                ,
                Imagen = datos.Element("IMAGEN").Value
            };

            x_director.Text = consulta.First().Director;
            x_distribuidor.Text = consulta.First().Distribuidor;
            x_argumento.Text = consulta.First().Argumento;
            x_precio.Text = consulta.First().Precio.ToString();

            x_imagen.Source = consulta.First().Imagen;


        }


    }
}
