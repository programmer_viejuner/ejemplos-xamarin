using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using XamarinAzure.Models;

namespace XamarinAzure.Helpers
{
    public class HelperDoctorAzure
    {
        private const string DireccionApi = "https://apicruddoctores.azurewebsites.net/";

        private HttpClient CrearCliente()
        {
            HttpClient clientehttp = new HttpClient();
            clientehttp.DefaultRequestHeaders.Accept.Clear();
            clientehttp.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return clientehttp;
        }


        public async Task<List<Doctor>> GetDoctores()
        {
            List<Doctor> listadatos = null;
            //CREAMOS LA PETICION
            String peticion = DireccionApi + "api/doctores";
            var uri = new Uri(string.Format(peticion, string.Empty));
            HttpClient client = CrearCliente();
            var respuesta = await client.GetAsync(uri);
            if (respuesta.IsSuccessStatusCode)
            {
                var contenido = await respuesta.Content.ReadAsStringAsync();
                listadatos = JsonConvert.DeserializeObject<List<Doctor>>(contenido);
            }
            return listadatos;
        }


    }
}
