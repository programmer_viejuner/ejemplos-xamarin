using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinAzure.Views;

namespace XamarinAzure
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new TodosDoctoresView();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
