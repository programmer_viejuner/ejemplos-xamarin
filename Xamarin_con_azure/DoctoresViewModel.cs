using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using XamarinAzure.Helpers;
using XamarinAzure.Models;
using XamarinAzure.ViewModels.Base;

namespace XamarinAzure.ViewModels
{
    public class DoctoresViewModel : ViewModelBase
    {
        HelperDoctorAzure helper;

        public DoctoresViewModel()
        {
            helper = new HelperDoctorAzure();
            Task.Run(async () => {
                List<Doctor> lista = await helper.GetDoctores();
                this.Doctores = new ObservableCollection<Doctor>(lista);
            });
        }

        private ObservableCollection<Doctor> _Doctores;

        public ObservableCollection<Doctor> Doctores
        {
            get { return this._Doctores; }
            set
            {
                this._Doctores = value;
                OnPropertyChanged("Doctores");
            }
        }

    }

}
