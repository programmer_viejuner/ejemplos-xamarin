using System;
using Realms;

namespace xamarin_realm_examen.Models
{
    public class Departamento : RealmObject
    {
        public int Empleados { get; set; }
        public String Dept_no { get; set; }
        public String Dnombrec { get; set; }
        public String Loc { get; set; }
    }
}
