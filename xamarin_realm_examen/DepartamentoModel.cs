using System;
using Xamarin.Forms;
using xamarin_realm_examen.Models;
using xamarin_realm_examen.Repositories;
using xamarin_realm_examen.ViewModels.Base;

namespace xamarin_realm_examen.ViewModels
{
    public class DepartamentoModel : ViewModelBase
    {
        RepositoryRealm repo;

        public DepartamentoModel()
        {
            this.repo = new RepositoryRealm();
            this.Departamento = new Departamento();
        }

        //PROPIEDAD PARA REALIZAR LOS BINDINGS SOBRE LAS VISTAS 
        private Departamento _Departamento;
        public Departamento Departamento
        {
            get { return this._Departamento; }
            set
            {
                this._Departamento = value;
                OnPropertyChanged("Departamento");
            }
        }

        //PROPIEDAD PARA MOSTRAR LOS RESULTADOS DE LAS ACCIONES 
        private String _Mensaje;
        public String Mensaje
        {
            get { return this._Mensaje; }
            set
            {
                this._Mensaje = value;
                OnPropertyChanged("Mensaje");
            }
        }

        public Command InsertarDato
        {
            get
            {
                return new Command(() =>
                {
                    this.repo.InsertarDepartamento(this.Departamento);
                    this.Mensaje = "Dato insertado";
                });
            }
        }


    }
}
