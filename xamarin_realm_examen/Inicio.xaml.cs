using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using xamarin_realm_examen.Repositories;

namespace xamarin_realm_examen.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Inicio : ContentPage
    {
        RepositoryRealm repo;

        public Inicio()
        {
            InitializeComponent();
            this.repo = new RepositoryRealm();
            this.botondetalles.Clicked += Botondetalles_Clicked;
            
            this.botoninsertar.Clicked += Botoninsertar_Clicked;
            
            this.botonmostrarregistros.Clicked += Botonmostrarregistros_Clicked;
        }

        private async void Botoninsertar_Clicked(object sender, EventArgs e)
        {
            InsertarDepartamento insertview = new InsertarDepartamento();
            await Navigation.PushModalAsync(insertview);
        }

        private async void Botonmostrarregistros_Clicked(object sender, EventArgs e)
        {
            DepartamentosView listaview = new DepartamentosView();
            await Navigation.PushModalAsync(listaview);
        }

        private async void Botondetalles_Clicked(object sender, EventArgs e)
        {
            //DetallesDepartamento detailsview = new DetallesDepartamento();
            //DepartamentoModel viewmodel = new DepartamentoModel();
            //int id = int.Parse(this.txtid.Text);
            //Departamento depar = this.repo.BuscarPersonaje(id);
            //viewmodel.Personaje = person;
            //detailsview.BindingContext = viewmodel;
            //await Navigation.PushModalAsync(detailsview);
        }

        
    }
}
