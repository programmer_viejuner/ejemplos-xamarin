using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using xamarin_realm_examen.Models;
using xamarin_realm_examen.Repositories;
using xamarin_realm_examen.ViewModels.Base;

namespace xamarin_realm_examen.ViewModels
{
    public class DepartamentosViewModel : ViewModelBase
    {
        private RepositoryRealm repo;

        public DepartamentosViewModel()
        {
            this.repo = new RepositoryRealm();
            List<Departamento> lista = this.repo.GetDepartamentos();
            this.Departamentos = new ObservableCollection<Departamento>(lista);
        }


        private ObservableCollection<Departamento> _Departamentos;

        public ObservableCollection<Departamento> Departamentos
        {
            get { return this._Departamentos; }
            set
            {
                this._Departamentos = value;
                OnPropertyChanged("Departamentos");
            }
        }
    }
}
