using System;
using System.Collections.Generic;
using System.Linq;
using Realms;
using xamarin_realm_examen.Models;

namespace xamarin_realm_examen.Repositories
{
    public class RepositoryRealm
    {
        private Realm conexionrealm;
        Transaction transaction;

        public RepositoryRealm()
        {
            //CREAMOS EL OBJETO QUE NOS PERMITIRA CONECTARNOS 
            //CON REALM EN CADA DISPOSITIVO 
            this.conexionrealm = Realm.GetInstance();
        }

        //METODO PARA DEVOLVER TODOS LOS OBJETOS DEPARTAMENTO
        public List<Departamento> GetDepartamentos()
        {
            List<Departamento> lista = this.conexionrealm.All<Departamento>().ToList();
            return lista;
        }

        

        public int GetMaximoIdDepartamento()
        {
            //RECUPERAMOS TODOS LOS DEPARTAMENTOS 
            List<Departamento> lista = this.GetDepartamentos();
            return lista.Count + 1;
        }

        //METODO PARA INSERTAR EN REALM.   
        public void InsertarDepartamento(Departamento departamento)
        {
            transaction = conexionrealm.BeginWrite();
            var entry = conexionrealm.Add(departamento);
            transaction.Commit();
            //this.conexionrealm.Write(() => 
            //{ 
            //    //CREAMOS UN NUEVO PERSONAJE PARA INSERTAR 
            //    //EN EL METODO WRITE 
            //    Personaje newpersonaje = new Personaje(); 
            //    newpersonaje.IdPersonaje = this.GetMaximoIdPersonaje(); 
            //    newpersonaje.Nombre = personaje.Nombre; 
            //    newpersonaje.Serie = personaje.Serie; 
            //}); 
        }

        
    }
}
