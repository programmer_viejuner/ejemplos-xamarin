using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using xamarin_realm.Models;
using xamarin_realm.Repositories;

namespace xamarin_realm.ViewModels
{
    public class PersonajesViewModel : ViewModelBase
    {
        private RepositoryRealm repo;

        public PersonajesViewModel()
        {
            this.repo = new RepositoryRealm();
            List<Personaje> lista = this.repo.GetPersonajes();
            this.Personajes = new ObservableCollection<Personaje>(lista);
        }


        private ObservableCollection<Personaje> _Personajes;

        public ObservableCollection<Personaje> Personajes
        {
            get { return this._Personajes; }
            set
            {
                this._Personajes = value;
                OnPropertyChanged("Personajes");
            }
        }
    }
}
