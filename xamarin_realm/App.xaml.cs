using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using xamarin_realm.Views;

namespace xamarin_realm
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            //MainPage = new MainPage();
            MainPage = new Inicio();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
