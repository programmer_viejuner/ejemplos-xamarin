using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using xamarin_xml.Models;
using xamarin_xml.Views;

namespace xamarin_xml.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AlumnosView : ContentPage
    {
        public AlumnosView()
        {
            InitializeComponent();
            this.botonleeralumnos.Clicked += Botonleeralumnos_Clicked;
        }

        private void Botonleeralumnos_Clicked(object sender, EventArgs e)
        {
            var assembly = IntrospectionExtensions.GetTypeInfo(typeof(AlumnosView)).Assembly;
            Stream stream = assembly.GetManifestResourceStream("xamarin_xml.Ficheros.alumnos.xml");
            string contenido = "";
            using (var reader = new System.IO.StreamReader(stream))
            {
                contenido = reader.ReadToEnd();
            }

            XDocument docxml = XDocument.Parse(contenido);
            var consulta = from datos in docxml.Descendants("alumno")
                           select new Alumno
                           {
                               IdAlumno = int.Parse(datos.Element("idalumno").Value)
                               ,
                               Nombre = datos.Element("nombre").Value
                               ,
                               Apellidos = datos.Element("apellidos").Value
                               ,
                               Nota = int.Parse(datos.Element("nota").Value)
                           };
            List<Alumno> alumnos = consulta.ToList();
            this.lsvalumnos.ItemsSource = alumnos;
        }
    }
}
