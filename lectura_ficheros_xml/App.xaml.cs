using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using xamarin_xml.Views;

namespace xamarin_xml
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new AlumnosView();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
