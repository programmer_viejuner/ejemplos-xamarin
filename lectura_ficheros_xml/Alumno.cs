using System;
using System.Collections.Generic;
using System.Text;
namespace xamarin_xml.Models
{
    public class Alumno
    {
        public int IdAlumno { get; set; }
        public String Nombre { get; set; }
        public String Apellidos { get; set; }
        public int Nota { get; set; }
    }
}
