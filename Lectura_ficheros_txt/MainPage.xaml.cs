using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace xamarin_ficheros
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            this.botonleerfichero.Clicked += Botonleerfichero_Clicked;
        }

        private void Botonleerfichero_Clicked(object sender, EventArgs e)
        {
            var assembly = IntrospectionExtensions.GetTypeInfo(typeof(MainPage)).Assembly;
            Stream stream = assembly.GetManifestResourceStream("xamarin_ficheros.Ficheros.documento.txt");
            string contenido = "";
            using (var reader = new System.IO.StreamReader(stream))
            {
                contenido = reader.ReadToEnd();
            }
            this.lbldatos.Text = contenido;
        }
    }
}
