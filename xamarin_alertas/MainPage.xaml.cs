using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace xamarin_alertas
{

    public partial class MainPage : ContentPage
    {

        public MainPage()
        {
            InitializeComponent();
            this.botonsimple.Clicked += (sender, args) =>
            {
                DisplayAlert("Alerta", "Mensaje simple", "OK");
            };
            this.botonokcancel.Clicked += Botonokcancel_Clicked;
            this.botonopciones.Clicked += Botonopciones_Clicked;
            this.botonmodal.Clicked += Botonmodal_Clicked;
        }

        private async void Botonokcancel_Clicked(object sender, EventArgs e)
        {
            bool respuesta = await DisplayAlert("Pregunta...", "¿Real Madrid campeón de Europa?", "Si", "No");
            if (respuesta == true)
            {
                this.respuesta.Text = "Tú si que sabes...";
            }
            else
            {
                this.respuesta.Text = "No tienes ni idea...";
            }
        }

        private async void Botonopciones_Clicked(object sender, EventArgs e)
        {
            String opcion = await DisplayActionSheet("¿Campeón de la Champions?", "Cancelar"
                , "Barcelona... jajaja", "Real Madrid", "Liverpool", "Atletico de Madrid?");
            this.respuesta.Text = opcion;
        }

        private async void Botonmodal_Clicked(object sender, EventArgs e)
        {
            paginamodal modal = new paginamodal();
            await Navigation.PushModalAsync(modal);
        }


    }
}
